Write a function to add 3 numbers.
#include <stdio.h>

void input(int *a,int *b,int *c)
{
    printf("Enter the three numbers\n");
    scanf("%d%d%d",a,b,c);
}

int compute(int a,int b,int c)
{
    int sum;
    sum = a+b+c;
    return sum;
}

void output(int sum)
{
    printf("sum = %d\n",sum);
}
 
int main()
{
    int a,b,c,sum;
    input(&a,&b,&c);
    sum=compute(a,b,c);
    output(sum);
    return 0;
}



